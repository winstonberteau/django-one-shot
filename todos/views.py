from django.shortcuts import render, get_object_or_404
from todos.models import TodoList, TodoItem

# Create your views here.


def todo_list_list(request):
    todo_list = TodoList.objects.all()
    context = {
        "todo_list": todo_list,
    }
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    todo_item = get_object_or_404(TodoItem, id=id)
    context = {
        "todo_item": todo_item,
    }
    return render(request, "todos/detail.html", context)
